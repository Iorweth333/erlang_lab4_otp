%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. maj 2017 12:20
%%%-------------------------------------------------------------------
-module(pollution_server_otp).
-author("Karol").
-behaviour(gen_server).
-version('1.0').

-export([start_link/0, init/1]).
-export([addStation/2, addValue/4, removeValue/3, getOneValue/3, getStationMean/2]).
-export ([getDailyMean/2, getAreaMean/3, crash/0, handle_call/3]).

start_link() ->
  InitialValue = 0,
  gen_server:start_link(
    {local,?MODULE},
    ?MODULE,
    InitialValue, []).

init(_InitialValue) ->
  {ok, pollution:createMonitor()}.


%User Interface

addStation (Nazwa, Wspolrzedne) when is_list(Nazwa), is_tuple(Wspolrzedne)->
  gen_server:call (pollution_server_otp, {addStation, Nazwa, Wspolrzedne}).

addValue (Nazwa, Data, TypPomiaru, Wartosc) when is_list(Nazwa), length(Nazwa) > 0 ->
  gen_server:call (pollution_server_otp, {addValue, Nazwa, Data, TypPomiaru, Wartosc});

addValue (Wspolrzedne, Data, TypPomiaru, Wartosc) when is_tuple (Wspolrzedne) ->
  gen_server:call (pollution_server_otp, {addValue, Wspolrzedne, Data, TypPomiaru, Wartosc}).

removeValue (Nazwa, Czas, TypPomiaru) when is_list(Nazwa) ->
  gen_serwer:call (pollution_server_otp, {removeValue, Nazwa, Czas, TypPomiaru});

removeValue (Wspolrzedne, Czas, TypPomiaru) when is_tuple(Wspolrzedne) ->
  gen_serwer:call (pollution_server_otp, {removeValue, Wspolrzedne, Czas, TypPomiaru}).

getOneValue (Nazwa, Czas, TypPomiaru) when is_list(Nazwa) ->
  gen_serwer:call (pollution_server_otp, {getOneValue, Nazwa, Czas, TypPomiaru});

getOneValue (Wspolrzedne, Czas, TypPomiaru) when is_tuple(Wspolrzedne) ->
  gen_serwer:call (pollution_server_otp, {getOneValue, Wspolrzedne, Czas, TypPomiaru}).

getStationMean (Nazwa, TypPomiaru) when is_list(Nazwa) ->
  gen_serwer:call (pollution_server_otp, {getStationMean, Nazwa, TypPomiaru});

getStationMean (Wspolrzedne, TypPomiaru) when is_tuple (Wspolrzedne) ->
  gen_serwer:call (pollution_server_otp, {getStationMean, Wspolrzedne, TypPomiaru}).

getDailyMean (TypPomiaru, Data) ->
  gen_serwer:call (pollution_server_otp, {getDailyMean, TypPomiaru, Data}).

getAreaMean (Nazwa, TypPomiaru, Promien) when is_list(Nazwa) ->
  gen_serwer:call (pollution_server_otp, {getAreaMean, Nazwa,TypPomiaru, Promien});

getAreaMean (Wspolrzedne, TypPomiaru, Promien) when is_tuple(Wspolrzedne) ->
  gen_serwer:call (pollution_server_otp, {getAreaMean, Wspolrzedne,TypPomiaru, Promien}).

crash () -> 1/0.

%callbacks

handle_call ({addStation, Nazwa, Wspolrzedne}, _From, Value) when is_list(Nazwa), is_tuple(Wspolrzedne) ->
  M = pollution:addStation (Nazwa, Wspolrzedne, Value),
  {reply, M, M};     %  {reply, zwracana do klienta, nowy stan serwera}

handle_call ({addValue, NW, Data, TypPomiaru, Wartosc}, _From, Value) ->  %tu juz raczej nie musze sie przejmowac czy to nazwa czy wspolrzedne
  M = pollution:addValue (NW, Data, TypPomiaru, Wartosc, Value),
  {reply, M, M};

handle_call ([removeValue, NW, Czas, TypPomiaru], _From, Value) ->
  M = pollution:removeValue (NW, Czas, TypPomiaru, Value),
  {reply, M, M};

handle_call ([getOneValue, NW, Czas, TypPomiaru], _From, Value) ->
  M = pollution:getOneValue (NW, Czas, TypPomiaru, Value),
  {reply, M, Value};

handle_call ([getStationMean, NW, TypPomiaru], _From, Value) ->
  M = pollution:getStationMean (NW, TypPomiaru, Value),
  {reply, M, Value};

handle_call ([getDailyMean, TypPomiaru, Data], _From, Value) ->
  M = pollution:getDailyMean (TypPomiaru, Data, Value),
  {reply, M, Value};

handle_call ([getAreaMean, Wspolrzedne,TypPomiaru, Promien], _From, Value) ->
  M = pollution:getAreaMean (Wspolrzedne,TypPomiaru, Promien, Value),
  {reply, M, Value}.

%  to plus supervisor ZROBIC
% DOROBIĆ OBSŁUGĘ BŁĘDÓW!

%  c("src/pollution_server_otp.erl").
%  pollution_server_otp:start_link().
%  pollution_server_otp:addStation("Aleja Slowackiego", {50.2345, 18.3445}).
%  pollution_server_otp:addStation("Aleja Mickiewicza", {58.2345, 10.3445}).
%  pollution_server_otp:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60).
%  pollution_server_otp:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80).
%  pollution_server_otp:getDailyMean('PM10', {2017,4,23}).
