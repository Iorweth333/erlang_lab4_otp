%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. maj 2017 01:10
%%%-------------------------------------------------------------------
-module(pollution_server_supervisor).
-author("Karol").
-behaviour(supervisor).


%% API
-export([start_link/0, init/1]).

start_link () ->
  InitialValue = 0,
  supervisor:start_link({local, ?MODULE}, ?MODULE, InitialValue).

init (_InitialValue) ->
  RestartTuple = {one_for_one, 3, 200},
  ChildSpec = {
    pollution_server, %id do identyfikacji przez supervisora
    {pollution_server_otp, start_link, []},   %StartFunc = {moduł, funkcja zaczynająca, jej argumenty}
    permanent, %restart tylko po nienormalnym zakończeniu
    2000, %czas, jaki proces może poświęcić na zakończenie
    worker,
    [pollution_server_otp]
  },
  {ok, {RestartTuple, [ChildSpec]}}.




%  c("src/pollution_server_supervisor.erl").
%  pollution_server_supervisor:start_link().
%  pollution_server:addStation("Aleja Slowackiego", {50.2345, 18.3445}).
%  pollution_server:crash().
%  pollution_server:addStation("Aleja Slowackiego", {50.2345, 18.3445}).

%alternatywnie:

%  c("src/pollution_server_supervisor.erl").
%  pollution_server_supervisor:start_link().
%  pollution_server_otp:addStation("Aleja Slowackiego", {50.2345, 18.3445}).
%  pollution_server_otp:crash().
%  pollution_server_otp:addStation("Aleja Slowackiego", {50.2345, 18.3445}).

%ta wersja niby działa, ale nie wstaje po błędzie...