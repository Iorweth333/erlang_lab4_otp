%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. maj 2017 01:02
%%%-------------------------------------------------------------------
-module(pollution_server).
-author("Karol").

%% API
-export([start/0, stop/0, init/0, monitor_loop/1]).
-export([addStation/2, addValue/4, removeValue/3, getDailyMean/2]).
-export ([getOneValue/3, getStationMean/2, getAreaMean/3]).

start () ->
  register (monitor_s, spawn (?MODULE, init, [])).

init () ->
  P = pollution:createMonitor(),
  monitor_loop(P).

stop () ->
  monitor_s ! stop.


addStation (Nazwa, Wspolrzedne) ->
  monitor_s ! {self(),addStation, Nazwa, Wspolrzedne},
  receive
    M ->
      M, io:format ("Done: ~p~n", [M])
  end.


addValue (Nazwa, Data, TypPomiaru, Wartosc) when is_list(Nazwa), length(Nazwa) > 0->
  monitor_s ! {self(), addValue, Nazwa, Data, TypPomiaru, Wartosc},
  receive
    M ->
      M, io:format ("Done: ~p~n", [M])
  end;

addValue (Wspolrzedne, Data, TypPomiaru, Wartosc) when is_tuple(Wspolrzedne) ->
  monitor_s ! {self(), addValue, Wspolrzedne, Data, TypPomiaru, Wartosc},
  receive
    M ->
      M, io:format ("Done: ~p~n", [M])
  end.

removeValue (Nazwa, Czas, TypPomiaru) when is_list(Nazwa)->
  monitor_s ! {self(), Nazwa, Czas, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~p~n", [M])
  end;

removeValue (Wspolrzedne, Czas, TypPomiaru) when is_tuple(Wspolrzedne) ->
  monitor_s ! {self(), removeValue, Wspolrzedne, Czas, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~p~n", [M])
  end.

getOneValue (Wspolrzedne, Czas, TypPomiaru) when is_tuple(Wspolrzedne) ->
  monitor_s ! {self(), Wspolrzedne, Czas, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end;

getOneValue (Nazwa, Czas, TypPomiaru) when is_list(Nazwa) ->
  monitor_s ! {self(), Nazwa, Czas, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end.

getStationMean (Wspolrzedne, TypPomiaru) when is_tuple (Wspolrzedne) ->
  monitor_s ! {self(), getStationMean, Wspolrzedne, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end;

getStationMean (Nazwa, TypPomiaru) when is_list(Nazwa)->
  monitor_s ! {self(), getStationMean, Nazwa, TypPomiaru},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end.

getDailyMean (TypPomiaru, Data) ->
  monitor_s ! {self(), getDailyMean, TypPomiaru, Data},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end.

getAreaMean (Wspolrzedne, TypPomiaru, Promien) when is_tuple(Wspolrzedne) ->
  monitor_s ! {self(), getAreaMean, Wspolrzedne, TypPomiaru, Promien},
  receive
    M ->
      M, io:format ("Done: ~f~n", [M])
  end.
%  removeValue

%  getOneValue

%  getStationMean

%  getStationMean

%  getDailyMean

monitor_loop (P) ->   %dodać obsługę błędów
  receive
    stop -> ok;
    {PID, addStation, Nazwa, Wspolrzedne} ->
      case pollution:addStation(Nazwa, Wspolrzedne, P) of
        {error, _} -> monitor_loop (P);
        P2 -> PID ! P2,
              monitor_loop(P2)
      end;

    {PID, addValue, Nazwa, Data, TypPomiaru, Wartosc} when is_list(Nazwa), length(Nazwa) > 0 ->
      case pollution:addValue(Nazwa, Data, TypPomiaru, Wartosc, P) of
        {error, _} -> monitor_loop (P);
        P2 -> PID ! P2,
          monitor_loop(P2)
      end;

    {PID, addValue, Wspolrzedne, Data, TypPomiaru, Wartosc} when is_tuple(Wspolrzedne) ->
      case pollution:addValue(Wspolrzedne, Data, TypPomiaru, Wartosc, P) of
        {error, _} -> monitor_loop (P);
        P2 -> PID ! P2,
          monitor_loop(P2)
      end;

    {PID, removeValue, Nazwa, Czas, TypPomiaru} when is_list(Nazwa) ->
      case pollution:removeValue (Nazwa, Czas, TypPomiaru, P) of
        {error, _} -> monitor_loop (P);
        P2 -> PID ! P2,
          monitor_loop(P2)
      end;

    {PID, removeValue, Wspolrzedne, Czas, TypPomiaru} when is_tuple(Wspolrzedne) ->
      case pollution:removeValue (Wspolrzedne, Czas, TypPomiaru, P) of
        {error, _} -> monitor_loop (P);
        P2 -> PID ! P2,
          monitor_loop(P2)
      end;

    {PID, getOneValue, Wspolrzedne, Czas, TypPomiaru} when is_tuple(Wspolrzedne) ->
      case pollution:getOneValue (Wspolrzedne, Czas, TypPomiaru, P) of
        {error, _} -> monitor_loop (P);
        W -> PID ! W,
          monitor_loop(P)
      end;

    {PID, getStationMean, Nazwa, TypPomiaru} when is_list(Nazwa) ->
      case pollution:getStationMean (Nazwa, TypPomiaru, P) of
        {error, _} -> monitor_loop (P);
        W -> PID ! W,
          monitor_loop(P)
      end;

    {PID, getStationMean, Wspolrzedne, TypPomiaru} when is_tuple(Wspolrzedne) ->
      case pollution:getStationMean (Wspolrzedne, TypPomiaru, P) of
        {error, _} -> monitor_loop (P);
        W -> PID ! W,
          monitor_loop(P)
      end;

    {PID, getDailyMean, TypPomiaru, Data} ->
      case pollution:getDailyMean (TypPomiaru, Data, P) of
        {error, Message} -> PID ! Message,
          monitor_loop(P);
        W -> PID ! W,
          monitor_loop(P)
      end;

    {PID, getAreaMean, Wspolrzedne, TypPomiaru, Promien} when is_tuple(Wspolrzedne) ->
      case pollution:getAreaMean (Wspolrzedne, TypPomiaru, Promien, P) of
        {error, _} -> monitor_loop (P);
        W -> PID ! W,
          monitor_loop(P)
      end;

    _ ->
      unknownMessage,
      monitor_loop(P)
  end.


%  c("src/pollution_server.erl").
%  c("src/pollution.erl").
%  pollution_server:start().
%  pollution_server:addStation("Aleja Slowackiego", {50.2345, 18.3445}).
%  pollution_server:addStation("Aleja Mickiewicza", {58.2345, 10.3445}).
%  pollution_server:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60).
%  pollution_server:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80).
%  pollution_server:getDailyMean('PM10', {2017,4,23}).
%
%  pollution_server:stop().