%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%% FIRST ATTEMPT, DIDN'T WORK- PLEASE SEE INCREMENTATOR2
%%% @end
%%% Created : 16. maj 2017 02:30
%%%-------------------------------------------------------------------
-module(incrementator).
-author("Karol").

% API
-export([start_link/0, increment/1,get/1,close/1, loop/1]).

%% START %%
start_link()   -> {ok, spawn_link(?MODULE,loop,[0])}.

%% INTERFEJS KLIENT -> SERWER %%
%Tutaj należy coś doimplementować.
increment(PID) -> PID ! inc.
get(PID)       -> PID ! {get, self()}.  %%to jest bez sensu, bo jak podamy swoje PID, to wyśle z powrotem
                                        %% zaś jeśli genservera to on odeśle do siebie nie do nas...
                                        %%tak czy siak gdzies nastąpi wysłanie do samego siebie
close(PID)     -> PID ! terminate.

%% OBSŁUGA WIADOMOŚCI %%
loop(N) -> receive
             inc   -> loop(N+1);
             {get, PID} when is_pid(PID) -> PID ! N, loop(N);
             terminate  -> io:format("The number is: ~B~nBye.~n",[N]), ok;
             _ -> unknownMessage
           end.



%  c ("src/incrementator.erl").
%  {A, PID} = incrementator:start_link().
%  incrementator:increment(self()).
%  incrementator:get(self()).